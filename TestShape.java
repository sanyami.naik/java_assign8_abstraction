package Assignments.eight;

abstract class Shape{
   abstract void area(int parameter);
}

class Circle1 extends Shape{
    void area(int radius){
        System.out.println("The area of Circle is "+3.14*radius*radius);
    }
}

class Square extends Shape{
        void area (int length){
            System.out.println("The area of Square is "+length*length);
        }
}

class Cylinder extends Circle1{
    void area(int radius){
        int height=15;
        System.out.println("The area of Cylinder is "+(3.14*radius*radius*2)+2*3.14*radius*height);
    }
}

class Rectangle extends Square{
    void area(int length){
        int breadth=15;
        System.out.println("The area of Rectangle is "+length*breadth);
    }
}


public class TestShape {
    public static void main(String[] args) {
        Shape[] array=new Shape[4];
        Shape shape;

        shape=new Circle1();
        array[0]=shape;
        shape=new Square();
        array[1]=shape;
        shape=new Cylinder();
        array[2]=shape;
        shape=new Rectangle();
        array[3]=shape;

        for(Shape s:array)
        {
            s.area(25);
        }



    }
}


/*
OUTPUT:
The area of Circle is 1962.5
The area of Square is 625
The area of Cylinder is 3925.02355.0
The area of Rectangle is 375
 */