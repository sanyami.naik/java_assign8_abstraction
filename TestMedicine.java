package Assignments.eight;

import java.util.Random;

abstract class Medicine{
    void display(){
        System.out.println("Piramal HealthCare Centre ,Mahad 402309");
        System.out.println("Display of general medicine");
    }
}

class Tablets extends Medicine{
    void display(){
        System.out.println("Display of Tablets");
        System.out.println("Tablets to be Consumed only by adults");
    }
}

class Syrups extends Medicine{
    void display(){
        System.out.println("Display of Syrups");
        System.out.println("Syrups to for children");
    }
}

class Ointments extends Medicine{
    void display(){
        System.out.println("Display of Ointments");
        System.out.println("To be used for external use only");
    }
}
public class TestMedicine {
    public static void main(String[] args) {
        Medicine[] medicineArray=new Medicine[10];
        Medicine medicine;
        Random rand = new Random();

        for(int i=0;i<medicineArray.length;i++) {

            int randomVariable = rand.nextInt(3);
            if (randomVariable == 0) {
                medicine = new Tablets();
                medicineArray[i] = medicine;
                System.out.println("At "+i+"th index the object is of Tablets");
            }
            else if (randomVariable == 1)
            {
                medicine = new Syrups();
                medicineArray[i] = medicine;
                System.out.println("At "+i+"th index the object is of Syrups");
            }
            else
            {
                medicine = new Ointments();
                medicineArray[i] = medicine;
                System.out.println("At "+i+"th index the object is of Ointments");
            }


        }

        for(Medicine m:medicineArray){
            m.display();
        }



    }
}



/*
OUTPUT:

 C:\Users\Coditas\.jdks\corretto-1.8.0_342\bin\java.exe "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.2\lib\idea_rt.jar=54218:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.2\bin" -Dfile.encoding=UTF-8 -classpath C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\charsets.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\access-bridge-64.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\cldrdata.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\dnsns.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\jaccess.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\jfxrt.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\localedata.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\nashorn.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunec.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunjce_provider.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunmscapi.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunpkcs11.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\zipfs.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jce.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jfr.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jfxswt.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jsse.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\management-agent.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\resources.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\rt.jar;C:\Users\Coditas\IdeaProjects\Project\out\production\Project Assignments.eight.TestMedicine
At 0th index the object is of Ointments
At 1th index the object is of Syrups
At 2th index the object is of Tablets
At 3th index the object is of Tablets
At 4th index the object is of Tablets
At 5th index the object is of Tablets
At 6th index the object is of Syrups
At 7th index the object is of Tablets
At 8th index the object is of Ointments
At 9th index the object is of Ointments
Display of Ointments
To be used for external use only
Display of Syrups
Syrups to for children
Display of Tablets
Tablets to be Consumed only by adults
Display of Tablets
Tablets to be Consumed only by adults
Display of Tablets
Tablets to be Consumed only by adults
Display of Tablets
Tablets to be Consumed only by adults
Display of Syrups
Syrups to for children
Display of Tablets
Tablets to be Consumed only by adults
Display of Ointments
To be used for external use only
Display of Ointments
To be used for external use only

Process finished with exit code 0

 */