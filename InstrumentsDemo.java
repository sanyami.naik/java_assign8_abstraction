package Assignments.eight;

abstract class Instruments{
    abstract void play();
}

class Piano extends Instruments{
   void play(){
       System.out.println("Piano is playing tan tan tan");
   }
}

class Flute extends Instruments{
    void play(){
        System.out.println("Flute is playing tut tut tut");
    }
}

class Guitar extends Instruments{
    void play(){
        System.out.println("Guitar is playing tin tin tin");
    }
}
public class InstrumentsDemo {
    public static void main(String[] args) {
        Instruments[] Array=new Instruments[10];
        Instruments instruments;

        instruments=new Piano();
        Array[0]=instruments;

        instruments=new Guitar();
        Array[1]=instruments;

        instruments=new Flute();
        Array[2]=instruments;

        instruments=new Flute();
        Array[3]=instruments;

        instruments=new Piano();
        Array[4]=instruments;

        instruments=new Guitar();
        Array[5]=instruments;

        instruments=new Piano();
        Array[6]=instruments;

        instruments=new Guitar();
        Array[7]=instruments;

        instruments=new Flute();
        Array[8]=instruments;

        instruments=new Guitar();
        Array[9]=instruments;

        for(int i=0;i<Array.length;i++)
        {
            if(Array[i] instanceof Piano)
            {
                Array[i].play();
                System.out.println("The object at "+i+"th index is instance of Piano class ");
            }
            else if(Array[i] instanceof Guitar)
            {
                Array[i].play();
                System.out.println("The object at "+i+"th index is instance of Guitar class");
            }
            else
            {
                Array[i].play();
                System.out.println("The object at "+i+"th index is instance of Flute class ");
            }





        }

    }
}



/*Output:


C:\Users\Coditas\.jdks\corretto-1.8.0_342\bin\java.exe "-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.2\lib\idea_rt.jar=53494:C:\Program Files\JetBrains\IntelliJ IDEA Community Edition 2022.2\bin" -Dfile.encoding=UTF-8 -classpath C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\charsets.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\access-bridge-64.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\cldrdata.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\dnsns.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\jaccess.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\jfxrt.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\localedata.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\nashorn.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunec.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunjce_provider.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunmscapi.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\sunpkcs11.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\ext\zipfs.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jce.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jfr.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jfxswt.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\jsse.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\management-agent.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\resources.jar;C:\Users\Coditas\.jdks\corretto-1.8.0_342\jre\lib\rt.jar;C:\Users\Coditas\IdeaProjects\Project\out\production\Project Assignments.eight.InstrumentsDemo
Piano is playing tan tan tan
The object at 0th index is instance of Piano class
Guitar is playing tin tin tin
The object at 1th index is instance of Guitar class
Flute is playing tut tut tut
The object at 2th index is instance of Flute class
Flute is playing tut tut tut
The object at 3th index is instance of Flute class
Piano is playing tan tan tan
The object at 4th index is instance of Piano class
Guitar is playing tin tin tin
The object at 5th index is instance of Guitar class
Piano is playing tan tan tan
The object at 6th index is instance of Piano class
Guitar is playing tin tin tin
The object at 7th index is instance of Guitar class
Flute is playing tut tut tut
The object at 8th index is instance of Flute class
Guitar is playing tin tin tin
The object at 9th index is instance of Guitar class

Process finished with exit code 0


 */