package Assignments.eight;

abstract class Vehicle{
     abstract void engine();
}

 class Truck extends Vehicle{
     void engine(){
         System.out.println("Truck has a bad engine");
     }
 }
class Car extends Vehicle{
    void engine(){
        System.out.println("Car has a good engine");
    }
}
public class VehicleDemo {

    public static void main(String[] args) {
        Car car = new Car();
        car.engine();

        Truck truck = new Truck();
        truck.engine();

    }
}


/*

OUTPUT:
Car has a good engine
Truck has a bad engine

 */